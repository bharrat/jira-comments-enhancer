/**
 * Created with JetBrains PhpStorm.
 * User: bpatel
 * Date: 3/31/13
 * Time: 1:35 PM
 * To change this template use File | Settings | File Templates.
 */
/**
 * Created with JetBrains PhpStorm.
 * User: bpatel
 * Date: 3/30/13
 * Time: 10:57 AM
 * To change this template use File | Settings | File Templates.
 */

(function() {

    $(document).ready(function(){
        jiracomments.init();
        $('#close').bind('click', function() {
            console.log('closing popup');
            chrome.tabs.getSelected(null, function(tab) {
                chrome.tabs.update(tab.id, { selected: true } )
            });
        });
        jiracomments.getOptions(function(options) {
            if (options.jira_server) {
                $("#jira-server-url").attr("href", options.jira_server);
            } else {
                $("#jira-server-url").attr("href", "#");
            }



        });
    });


})();

