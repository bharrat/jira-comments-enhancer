var installedBefore = false;
if (!localStorage['installed']) {
    localStorage['installed'] = '1';
} else {
    installedBefore = true;
}
if (!installedBefore) {
    console.log('after install opening options.html');
    chrome.tabs.create({url: "options.html"});
} else {
    console.log('already installed');
}

chrome.tabs.onUpdated.addListener(function( tabId , info ) {
    if ( info.status == "complete" ) {
        chrome.storage.sync.get(function(options) {
            chrome.tabs.getSelected(null,function(tab) {
                var tablink = tab.url;
                if (options.jira_server && options.jira_server.trim() != "" && tablink.indexOf(options.jira_server.trim()) == 0) {
                    chrome.tabs.insertCSS(null, {file:"css/jira-comments.css"});
                    chrome.tabs.executeScript(null, {file: "js/lib/jquery.min.js"});
                    chrome.tabs.executeScript(null, {file: "js/jira-comments.js"});
                    chrome.tabs.executeScript(null, {code: "if(typeof JiraCommentNumberInjector != 'undefined') JiraCommentNumberInjector.init();"});
                }
            });


        });
    }
});

