// ==UserScript==
// @name           JIRA Comment Enhancer
// @namespace      https://v-web5.dev.qualys.com/
// @description    Insert comment number next to each comment in JIRA 
// @include        https://jira.intranet.qualys.com/*
// @downloadURL    https://v-web5.dev.qualys.com:5443/jira-comments.js
// @updateURL      https://v-web5.dev.qualys.com:5443/jira-comments.js
// @author         Bharat Patel ( bpatel@qualys.com) & Matt Wirges (mwirges@qualys.com)
// @version        1.1
// @require        http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js
// ==/UserScript==
//GM_addStyle(".q-comment { float:right;  color:red}");
(function() {
    var JiraCommentNumberInjector = {

        comment_class: 'activity-comment',
        page_id: 'jira',
        bugzillaServer: null,
        jiraServer:null,
        links: {},
        debug: false,

        init: function() {
            //load options asynchronously and run the script
            this.loadPreferencesAndRun();
        },

        _run: function() {
            if (this.shouldRun()) {
                this.log('Fixing comments for page:'+document.location.href);
                this.log(this);
                this._fixIt();
            } else {
                this.log('Not running on URL:'+document.location.href);
            }
        },

        loadPreferencesAndRun: function() {
            var _this = this;
            chrome.storage.sync.get(function(options) {
                if (options.jira_server && options.jira_server.trim() != "") {
                    _this.jiraServer = options.jira_server.trim();
                    if (options.bugzilla_server && options.bugzilla_server.trim() != "") {
                        _this.bugzillaServer = options.bugzilla_server.trim();
                    }
                    _this._run();
                } else {
                    this.log("Jira server URL not found. Not running");
                }

            });
        },

        shouldRun:function() {
            var canRun = false;
            if (this.jiraServer && document.location.href.indexOf(this.jiraServer) == 0) {
                canRun = true;
            }
            return canRun;
        },

        _fixIt: function () {

            if (document.body.id != "jira") return;
            var comments = $('div.'+this.comment_class);
            var i = 1;
            var _this = this;
            comments.each(function(index, comment) {
                var existingCN = $(comment).find('.q-comment');
                if (existingCN.length === 0) {
                    var actionLinks = $(this).find('.action-links');
                    var actionLink = actionLinks.find('a');
                    var chref = '';
                    var ctext = 'Comment #'+i++;
                    if (actionLink) {
                        chref=actionLink[0].href;
                        // assumption here is that older comments never have references to newer comments
                        var frag = chref.match(/\#comment\-[0-9]+$/);
                        _this.links[i-1] = frag;
                    }

                    $("<div class='q-comment'><a href='"+chref+"'>"+ctext+"</a></div>").insertBefore(actionLinks);

                    var actionBody = $(this).find('.action-body');

                    if (actionBody && typeof(actionBody[0]) !== 'undefined') {
                        _this.processCommentBody(actionBody[0], _this);
                    }
                }


            });

        },

        /**
         * processes the body of a comment, expecting a div with class .action-body
         *
         * Because this is expected to be called from a jQuery iterator, you use
         * "scope" instead of "this"
         */
        processCommentBody: function(commentDiv, scope) {

            // replace comment # references
            commentDiv.innerHTML = commentDiv.innerHTML.replace(/[Cc]omment\s*\#([0-9]+)/gm,
                function (match, p1, offset, string) {
                    if (typeof(scope.links[p1]) !== 'undefined') {
                        return "<a href='"+scope.links[p1]+"'>comment #"+p1+"</a>";
                    } // else
                    return match;
                }
            );
            if (scope.bugzillaServer && typeof scope.bugzillaServer !== 'undefined') {
                // replace bug # references
                commentDiv.innerHTML = commentDiv.innerHTML.replace(/bug\s*\#([0-9]+)/gm,
                    function (match, p1, offset, string) {
                        return "<a href='"+scope.bugzillaServer+"/show_bug.cgi?id="+
                        p1+"' target='_blank'>bug #"+p1+"</a>";
                    }
                );
            }

        },
        log: function(obj) {
            if (this.debug) {
                console.log(obj);
            }
        }
    };

    $(document).ready(function(){
        // in order to properly detect once Jira has finished updating
        // the issue panel so we can re-run the fixIt() method on the comments
        // we need to access the JavaScript context of the window itself (outside
        // of our plugin).
        JiraCommentNumberInjector.init();
        //to redraw comment numbers when user moves to a separate tab and then clicks back on Comments tab
        $('#activitymodule').on('click', function (event) {
            if (event.target.innerHTML == 'Comments') {
                setTimeout(function() {
                    JiraCommentNumberInjector.init()
                }, 1000);
                //JiraCommentNumberInjector.init();

            }
        });


    });

})();