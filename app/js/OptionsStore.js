/**
 * Created with JetBrains PhpStorm.
 * User: bpatel
 * Date: 3/31/13
 * Time: 10:47 AM
 * To change this template use File | Settings | File Templates.
 */

;(function() {

    //PRIVATE
    var debug = false;
    var storageType = null;
    //add items in the order of preferred storage type, when autodetecting
    var supportedStorageTypes = ['chromesync', 'chromelocalstorage', 'firefox'];
    var supported = false;
    var chromeStorageSave =  function(options, callback) {
        return chrome.storage.sync.set(options, callback);
    };
    var chromeStorageGet = function(callback) {
        return chrome.storage.sync.get(callback);
    }

    var firefoxSave  = function(options, callback) {
        this.options = options;
        if (callback && typeof callback == "function") {
            callback.call(this);
        }
    }
    var firefoxGet = function(callback, scope) {
        if (callback && typeof callback == "function") {
            callback.call(this, this.options||null);
        } else {
            return this.options || null;
        }
    }
    var storageTypeIsSupported = function(type) {

        type = type || storageType;

        var supported = false;
        switch (type) {
            case 'chromesync':
                if (chrome
                    && typeof chrome.storage != "undefined"
                    && typeof chrome.storage.sync != "undefined"
                    && typeof  chrome.storage.sync.get === "function"
                    && typeof  chrome.storage.sync.set === "function") {
                    supported = true;
                }
                break;
            case 'firefox':
                break;
            case 'chromelocalstorage':
                if (localStorage && typeof localStorage.getItem == "function" && typeof localStorage.setItem == "function") {
                    supported = true;
                }
                break;
        }

        return supported;
    }


    //PUBLIC METHODS
    this.init = function(type) {
        if (typeof type == "undefined") {
            if (debug) {
               console.log("No storage type specified, auto detecting supported storage type");
            }
            supported = false;
            for (var i=0; i < supportedStorageTypes.length;i++) {
                supported = storageTypeIsSupported(supportedStorageTypes[i]);
                if (supported) {
                    storageType = supportedStorageTypes[i];
                    if (debug) {
                        console.log("Found supported storage type.");
                        console.log("Setting storage type to:"+storageType);
                    }
                    break;
                }
            }
        } else {
            supported = false;
            if (debug) {
                console.log("Explicitly setting storage type to:"+type);
            }
            if (supportedStorageTypes.indexOf(type) === -1) {
                if (debug) {
                    console.log("Unsupported storage type");
                }
                throw "Unsupported storage type";
            } else {
                storageType = type;
            }
            supported = storageTypeIsSupported();
            if (!supported) {
                throw "Specified storage type is not supported by your browser";
            }
        }

    }



    this.saveOptions = function(options, callback) {
        if (!supported) {
            throw "Not supported with type:"+storageType;
        }
        switch(storageType) {
            case 'chromelocalstorage':

                break;
            case 'chromesync':
                return chromeStorageSave(options, callback);
                break;
            case 'firefox':
                break;
            default:
                //console.log('unsupported store');
        }
    }
    this.getOptions = function(callback) {
        if (!supported) {
            throw "Not supported with type:"+storageType;
        }
        switch(storageType) {
            case 'chromelocalstorage':
                break;
            case 'chromesync':
                return chromeStorageGet(callback);
                break;
            case 'firefox':
                break;
            default:
            //console.log('unsupported store');
        }
    }
    this.validateUrl =  function(url) {
        // now check if valid url
        // http://docs.jquery.com/Plugins/Validation/Methods/url
        // contributed by Scott Gonzalez: http://projects.scottsplayground.com/iri/
        return /^https?:\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&amp;'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&amp;'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&amp;'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&amp;'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&amp;'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(url);
    }

    jiracomments = this;

})();
