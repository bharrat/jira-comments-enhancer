/**
 * Created with JetBrains PhpStorm.
 * User: bpatel
 * Date: 3/30/13
 * Time: 10:57 AM
 * To change this template use File | Settings | File Templates.
 */

(function() {
    // Saves options to localStorage.
    function save_options() {
        var jira_server = $("#jiraServer").val();
        var bugzilla_server = $('#bugzillaServer').val();

        var options = {
            jira_server: jira_server,
            bugzilla_server: bugzilla_server
        };
        jiracomments.saveOptions(options, function() {
            // Update status to let user know options were saved.
            $('#status').show();
            setTimeout(function() {
                $('#status').hide();
                window.close();
            }, 750);
        });
    }

// Restores select box state to saved value from localStorage.
    function restore_options() {

        jiracomments.getOptions(function(options) {
            $("#jiraServer").val(options.jira_server);
            $("#bugzillaServer").val(options.bugzilla_server);

        });
    }

    function clearErrors (elem) {
        var controlGroup = $(elem).parents('.form-group');
        controlGroup.removeClass('error');
    }
    function markInvalid (elem, msg) {

        var msg = msg||"Please enter a valid value";
        var elem  = $(elem);
        var controlGroup = elem.parents('.form-group');
        controlGroup.addClass('error');

        elem.siblings('.help-inline').html(msg);

    }
    function validate_url_field(elem, required) {
        var elem = $(elem);
        var val = elem.val().trim();
        required = required || false;
        if (required && val == "") {
            $('#save').attr("disabled", true);
            markInvalid(elem, "This field is required.");
            return false;
        }
        if (val != "") {
            if(!/^https?:\/\//i.test(val)) {
                val = 'http://'+val; // set both the value
                $(elem).val(val); // also update the form element
            }
            var isValid = true;
            if (!jiracomments.validateUrl(val)) {
                isValid = false;
                markInvalid(elem, "Please enter a valid URL.");

                $('#save').attr("disabled", true);
            } else {
                $('#save').attr("disabled", false);
            }
        }
        return isValid;

    }

    $(document).ready(function(){
        //hide bugzilla option until we figure out way to access JIRA's JS
        //$('#bugzilla-option').hide();
        jiracomments.init();
        restore_options();
        $('#jiraServer').bind('blur', function(e){
            validate_url_field(e.target, true);
        });
        $('#jiraServer').bind('focus', function(e){
            clearErrors(e.target);
        });
        $('#bugzillaServer').bind('blur', function(e){
            validate_url_field(e.target, false);
        });
        $('#bugzillaServer').bind('focus', function(e){
            clearErrors(e.target);
        });
        $('#save').bind('click', save_options);
        $('#close').bind('click', function() {
            window.close();
        });
    });


})();

